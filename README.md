#INTERNAL USE ONLY


# LaravelSwagger
Swagger implementation for Laravel 5.x
This package uses [Swagger-spec 2.0](https://github.com/swagger-api/swagger-spec),  [Swagger-php 2.0](https://github.com/zircote/swagger-php/tree/2.x) and [Swagger-ui 2.1.1](https://github.com/swagger-api/swagger-ui/tree/v2.1.1) and provides developers an easy way to develop and test APIs
# Installation
# Configuration
