<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Laravel Swagger</title>
    <link href='vendor/laravel-swagger/custom/bootstrap/css/bootstrap.min.css' media='screen' rel='stylesheet' type='text/css'/>
    <link href='vendor/laravel-swagger/custom/bootstrap/css/bootstrap-theme.min.css' media='screen' rel='stylesheet' type='text/css'/>
    <link href='vendor/laravel-swagger/custom/css/app.css' media='screen' rel='stylesheet' type='text/css'/>
</head>
<body>
    @yield('content')
</body>
</html>